var express = require("express");

var app = express();

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));
/*
var staticResource = ["/public", "/bower_componets"];
for(var i in staticResource){
    app.use(express.static(__dirname + staticResource[i]))
}


*/
var port = parseInt(process.argv[2]) || 3000;
app.listen(port, function(){
    console.log("Application started at port %d", port);
})